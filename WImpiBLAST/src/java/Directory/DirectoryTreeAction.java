/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * This action class performs the functions of first time expanding the directory tree when user presses
 * Tree view button. This class is called only when Tree View button is pressed.
 */
package Directory;

import static Directory.DirectoryTreeAction.file_list;
import static Directory.DirectoryTreeAction.file_list_inodes;
import static Directory.DirectoryTreeAction.list;
import static Directory.DirectoryTreeAction.tree_contents;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import logic_core.Core_func;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import javax.servlet.http.HttpSession;

public class DirectoryTreeAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    public static ArrayList list = new ArrayList<String>();
    public static ArrayList tree_contents = new ArrayList<String>();
    public static String[] tree;
    public static ArrayList file_list = new ArrayList<String>();
    public static ArrayList file_list_inodes = new ArrayList<String>();
    public static ArrayList file_size_list = new ArrayList<String>();
    private static final String SUCCESS = "success";
    private static final String ERROR = "error";
    public static String path;

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        Core_func refssh = Core_func.getInstance();
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();
        String path = session.getAttribute("homedir").toString();
        Connection conn = null;
        Session sess = null;
        Session sess1 = null;
        Session list_files = null;
        Session list_files_inode = null;
        Session file_size = null;
        InputStream stdout = null;
        InputStream stdout1 = null;
        InputStream stdout_list_files = null;
        InputStream stdout_list_files_inode = null;
        InputStream stdout_file_size = null;
        BufferedReader br = null;
        BufferedReader br1 = null;
        BufferedReader br_list_files = null;
        BufferedReader br_list_files_inode = null;
        BufferedReader files_size = null;
        try {

            {

                int i = 0;

                String makecommand = " && find . -maxdepth 1 -not -path '*/\\.*' -type f -printf \"%T@ %Tc %p %i\\n\" | sort -nr | awk -F'/' '{print $NF}' | awk '{print $2}'";

                System.out.println("Command to find only files at depth 1 is :" + makecommand);
                conn = new Connection(hostname);

                conn.connect();

                boolean isAuthenticated = conn.authenticateWithPassword(username, password);

                if (isAuthenticated == false) {
                    throw new IOException("Authentication failed.");
                }

                sess = conn.openSession();
                sess1 = conn.openSession();
                list_files = conn.openSession();
                list_files_inode = conn.openSession();
                file_size = conn.openSession();

                sess.execCommand("cd " + path + "/" + " && ls -dt -- */ | awk -F'/' '{print $1}'");
                sess1.execCommand("cd " + path + "/" + " && ls -dt -i -- */ | awk -F' ' '{print $1}'");
                list_files.execCommand("cd " + path + "/" + " && find . -maxdepth 1 -not -path '*/\\.*' -type f -printf \"%T@ %Tc %p\\n\" | sort -nr | awk -F'/' '{print $NF}'");
                list_files_inode.execCommand("cd " + path + makecommand);
                file_size.execCommand("cd " + path + " && find . -maxdepth 1 -not -path '*/\\.*' -type f -printf \"%T@ %T@ %p %k KB\\n\" | sort -nr | awk -F'/' '{print $NF}'|awk '{print $2\" \"$3}'");
                
                stdout = new StreamGobbler(sess.getStdout());
                stdout1 = new StreamGobbler(sess1.getStdout());
                stdout_list_files = new StreamGobbler(list_files.getStdout());
                stdout_list_files_inode = new StreamGobbler(list_files_inode.getStdout());
                stdout_file_size = new StreamGobbler(file_size.getStdout());

                br = new BufferedReader(new InputStreamReader(stdout));
                br1 = new BufferedReader(new InputStreamReader(stdout1));
                br_list_files = new BufferedReader(new InputStreamReader(stdout_list_files));
                br_list_files_inode = new BufferedReader(new InputStreamReader(stdout_list_files_inode));
                files_size = new BufferedReader(new InputStreamReader(stdout_file_size));

                list.clear();
                tree_contents.clear();
                file_list.clear();
                file_list_inodes.clear();
                file_size_list.clear();

                while (true) {


                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }

                    list.add(line);
                   
                    i++;
                }

                i = 0;
                //System.out.println(list.size());

                System.out.println("ExitCode: " + sess.getExitStatus());

                while (true) {


                    String line1 = br1.readLine();
                    if (line1 == null) {
                        break;
                    }
                    tree_contents.add(line1);
                   // System.out.println(tree_contents.get(i));
                    i++;
                }

                i = 0;
                //System.out.println(tree_contents.size());

                System.out.println("ExitCode: " + sess1.getExitStatus());

                while (true) {


                    String line_list_files = br_list_files.readLine();
                    if (line_list_files == null) {
                        break;
                    }

                    file_list.add(line_list_files);
                    //tree_contents.add(line);
                    //System.out.println(file_list.get(i));
                    i++;
                }
                i = 0;
                //System.out.println(file_list.size());

                System.out.println("ExitCode: " + list_files.getExitStatus());

                while (true) {

                    String line_for_files_inode = br_list_files_inode.readLine();
                    if (line_for_files_inode == null) {
                        break;
                    }

                    file_list_inodes.add(line_for_files_inode);
                    //tree_contents.add(line);
                    //System.out.println("file_inodes"+file_list_inodes.get(l));
                    i++;
                }

                //System.out.println(file_list_inodes.size());
                System.out.println("ExitCode: " + list_files_inode.getExitStatus());

                while (true) {


                    String line = files_size.readLine();
                    if (line == null) {
                        break;
                    }

                    file_size_list.add(line);
                    //tree_contents.add(line);
                    //System.out.println(file_size_list.get(i));
                    i++;
                }

                //System.out.println(list.size());

                System.out.println("ExitCode: " + file_size.getExitStatus());
                sess.close();
                sess = null;
                sess1.close();
                sess1 = null;
                list_files.close();
                list_files = null;
                list_files_inode.close();
                list_files_inode = null;
                file_size.close();
                file_size = null;
                conn.close();
                conn = null;
                stdout.close();
                stdout1.close();
                stdout_list_files.close();
                stdout_list_files_inode.close();
                br.close();
                br1.close();
                br_list_files.close();
                br_list_files_inode.close();

            }

        } catch (IOException e) {
            System.out.println("------------------------------------------------");
            System.out.println("Inside exceptionn code of directory--tree--action--class");
            System.out.println("------------------------------------------------");
            sess.close();
            sess1.close();
            list_files.close();
            list_files_inode.close();
            file_size.close();
            file_size = null;
            conn.close();
            stdout.close();
            stdout1.close();
            stdout_list_files.close();
            stdout_list_files_inode.close();
            stdout_file_size.close();
            br.close();
            br1.close();
            br_list_files.close();
            br_list_files_inode.close();
            files_size.close();
            list.clear();
            tree_contents.clear();
            file_list.clear();
            file_list_inodes.clear();
            file_size_list.clear();
            e.printStackTrace(System.err);
            return mapping.findForward(ERROR);

        }

        sess = null;
        sess1 = null;
        list_files = null;
        list_files_inode = null;
        conn = null;
        return mapping.findForward(SUCCESS);
    }
}
