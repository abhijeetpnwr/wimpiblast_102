/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file checks for valid session and if session timeout has occured then it redirects user to
 * login page to perform login again to continue operations.
 */
package sessionfilter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author parichit
 */
public class sessionfilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession sess = req.getSession();

        System.out.println("inside session filter");

        if (sess.getAttribute("username") == null) {
            System.out.println("Entered Into IF clause of session check------ Session is invalid now");
            res.sendRedirect(req.getContextPath() + "/welcome/loginpage.jsp");

        } else if (!sess.getAttribute("username").equals("root")) {
            
            
            System.out.println("Session is valid on this request");
            if (req instanceof HttpServletRequest) {
               
                String ae = req.getHeader("accept-encoding");
                if (ae != null && ae.indexOf("gzip") != -1) {
                    //System.out.println("GZIP supported, compressing.");
                    GZIPResponseWrap wrappedResponse =
                            new GZIPResponseWrap((HttpServletResponse) response);
                    chain.doFilter(req, wrappedResponse);
                    wrappedResponse.completeResponse();
                    return;
                }

               chain.doFilter(req, res);
            }

        } else if (sess.getAttribute("username").equals("root")) {

            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
            System.out.println("Admin trying to access user area-- rediected to admin home page");
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
            res.sendRedirect(req.getContextPath() + "/administrator/Home/home.jsp");

        }
        // chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        
        String sessionParam = config.getInitParameter("test-param");
        
        System.out.println("Test Param: " + sessionParam);
    }

    @Override
    public void destroy() {
       
    }
}
