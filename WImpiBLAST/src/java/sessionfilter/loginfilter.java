/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * This file is very critical from security point of view as it checks the user sessions and 
 * does not allow multiple logins from same user. This file mainly checks for presance of username
 * and if present , it automatically redirects the user to home page, otherwise users are sent to
 * login page to verify credentials and perform login.
 */
package sessionfilter;

import extralogics.Mycounter;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author parichit
 */
public class loginfilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession sess = req.getSession();

        System.out.println("inside login filter");

        if (sess.getAttribute("username") == null)
        {
            System.out.println("reachd chk 0");
            Mycounter count= new Mycounter();
            System.out.println("reachd chk 1");
            Enumeration<String> servlet_attr=request.getServletContext().getAttributeNames();
            Boolean b=false;
            
            while(servlet_attr.hasMoreElements())
            {
                String name=servlet_attr.nextElement();
                if(name.equals("counter"))
                {
                    b=true;
                    break;
                }
          
            }
            
            if(b==true)
            {
                System.out.println("counter value exists so no need to reset it");      
                              
            }
            
            else
            {
                
                System.out.println("need to set counter value");
                int counter=count.getvisitors();
                System.out.println("value of counter from text file is"+counter);
                req.getServletContext().setAttribute("counter",counter);
                
            }
            
            
            System.out.println("reachd chk 2");
          
            System.out.println("Session is invalid on login------ redirecting to login page");
            res.sendRedirect(req.getContextPath() + "/welcome/loginpage.jsp");
            
        } else if (sess.getAttribute("username").equals("root")) {
            System.out.println("Session is valid on this request-- redirecting to admin home page");
            res.sendRedirect(req.getContextPath() + "/administrator/Home/home.jsp");
        } else {
            System.out.println("Session is valid on user login-----Redirecting to user home page");
            res.sendRedirect(req.getContextPath() + "/Home/home.jsp");
        }

        // chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        String loginParam = config.getInitParameter("test-param");
        System.out.println("Test Param: " + loginParam);
    }

    @Override
    public void destroy() {
       
    }
}
