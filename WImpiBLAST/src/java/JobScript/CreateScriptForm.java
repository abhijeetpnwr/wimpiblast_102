/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JobScript;

/**
 *
 * @author parichit
 */
public class CreateScriptForm extends org.apache.struts.action.ActionForm {
    
    
    private String scriptname= "";
    private String jobname = "";
    private String wtime = "";
    private String nnodes = "";
    private String ppn = "";
    private String start = "";
    private String end = "";
    private String abort = "";
    private String emailid = "";
    

    public String getScriptname() {
        return scriptname;
    }

    public void setScriptname(String scriptname) {
        this.scriptname = scriptname;
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public String getWtime() {
        return wtime;
    }

    public void setWtime(String wtime) {
        this.wtime = wtime;
    }

    public String getNnodes() {
        return nnodes;
    }

    public void setNnodes(String nnodes) {
        this.nnodes = nnodes;
    }

    public String getPpn() {
        return ppn;
    }

    public void setPpn(String ppn) {
        this.ppn = ppn;
    }

   
    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getAbort() {
        return abort;
    }

    public void setAbort(String abort) {
        this.abort = abort;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

  
}
