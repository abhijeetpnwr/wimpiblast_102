/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JobScript;

/**
 *
 * @author parichit
 */
public class ModifyScriptForm extends org.apache.struts.action.ActionForm {
    
    
    private String scriptpath ;
    private String editedscript="" ;
    private String button ;
    private String newname="";

    public String getNewname() {
        return newname;
    }

    public void setNewname(String newname) {
        this.newname = newname;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }
    

    public String getEditedscript() {
        return editedscript;
    }

    public void setEditedscript(String editedscript) {
        this.editedscript = editedscript;
    }
    
    

    public String getScriptpath() {
        return scriptpath;
    }

    public void setScriptpath(String scriptpath) {
        this.scriptpath = scriptpath;
    }

    }

