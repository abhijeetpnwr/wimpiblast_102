

/*for fat
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file creates a script in association with create script module os WImpiBLAST portal. The second
 * part of script phase is mpiBlast action file found in mpiBlast package. These two files 
 * together enters the torque relevant and mpiblast specific data into created script.
 */
package JobScript;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logic_core.Core_func;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author parichit
 */
public class CreateScriptAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String CREATE_SUCCESS = "success";
    private static final String CREATE_FAIL = "fail";
  

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        CreateScriptForm myform = (CreateScriptForm) form;
        String scriptname = myform.getScriptname();
        String jobname = myform.getJobname();
        String wtime = myform.getWtime();
        String nnodes = myform.getNnodes();
        String ppn = myform.getPpn();
        String start = myform.getStart();
        String end = myform.getEnd();
        String abort = myform.getAbort();
        String emailid = myform.getEmailid();
        Core_func refssh = Core_func.getInstance();
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();
        String homedir = session.getAttribute("homedir").toString();
        String Status = null;

        try {
            
            

            Status = refssh.createnewFile(hostname, username, password, scriptname, jobname, wtime, nnodes, ppn, start, end, abort, emailid, homedir);
            if (Status.equals("FileCreationConversionSuccess") | Status.equals("FileCreationSuccessConversionFail")) {

                session.setAttribute("scriptname", scriptname);
                session.setAttribute("nnodes", nnodes);
                session.setAttribute("ppn", ppn);
                session.setAttribute("jobname", jobname);
                System.out.println("file create successfully");
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                System.out.println("Inside create script action class, scriptname, no. of nodes and Process per node is: " + scriptname + " " + nnodes + " " + ppn);
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                return mapping.findForward(CREATE_SUCCESS);
            } else if (Status.equals("JobFldrExists")) {
                session.setAttribute("action_stat", "fldrexst");
                return mapping.findForward(CREATE_FAIL);

            } else if (Status.equals("FldrCreationExcep")) {
                session.setAttribute("action_stat", "fldrexcep");
                return mapping.findForward(CREATE_FAIL);
                
            }else if (Status.equals("dataappendingexception")) {
                session.setAttribute("action_stat", "dtaexcep");
                return mapping.findForward(CREATE_FAIL); 
                
            } else if (Status.equals("FileExists")) {
                session.setAttribute("action_stat", "fileexst");
                return mapping.findForward(CREATE_FAIL);

            } else if (Status.equals("PermissionConflicts")) {
                session.setAttribute("action_stat", "prmcnflct");
                return mapping.findForward(CREATE_FAIL);

            } else {

                session.setAttribute("action_stat", "no");
                session.removeAttribute("scriptname");
                session.removeAttribute("nnodes");
                session.removeAttribute("ppn");
                session.removeAttribute("jobname");
                return mapping.findForward(CREATE_FAIL);
            }

        } catch (Exception e) {

            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside exception of Create--Script--Action");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            session.removeAttribute("scriptname");
            session.removeAttribute("nnodes");
            session.removeAttribute("ppn");
            session.removeAttribute("jobname");
            session.setAttribute("action_stat", "ex");
            return mapping.findForward(CREATE_FAIL);


        }

    }
}
