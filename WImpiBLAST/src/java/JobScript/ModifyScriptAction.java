/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file is responsible for modify script status reporting only. Since actual file display is 
 * done with help of asynchronius ajax calls not through post requests to server hence this file 
 * just tell user whether script modification was successful or not. Tjis is not responsible for
 * displaying of script but does editing of script by replacing original content with user entered 
 * content from text area.
*/

package JobScript;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import logic_core.Core_func;

/**
 *
 * @author parichit
 */
public class ModifyScriptAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String MODIFY_SUCCESS = "success";
    private static final String MODIFY_FAIL = "fail";

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        ModifyScriptForm editedform = (ModifyScriptForm) form;

        String scriptpath = editedform.getScriptpath();
        String editedscript = editedform.getEditedscript();
        String button = editedform.getButton();
        String newname = editedform.getNewname();
        String onlyscriptname = null;
        String pathwidnewname = null;
        int i = 0, j = 0;
        HttpSession session = request.getSession();
        Core_func refssh = Core_func.getInstance();
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();
        System.out.println("button value is" + button);
        String homedir = session.getAttribute("homedir").toString();


        try {

            if (button.equals("SAVE SCRIPT")) {
                if ("".equals(newname)) {

                    if (refssh.editFile(hostname, username, password, editedscript.trim(), scriptpath.trim(),homedir)) {

                        System.out.println("------------------------------------------------------------------");
                        System.out.println("In modify script action class: Script Modification Successful");
                        System.out.println("------------------------------------------------------------------");
                        session.setAttribute("action_stat", "yes");
                        return mapping.findForward(MODIFY_SUCCESS);
                    } else {

                        session.setAttribute("action_stat", "no");
                        return mapping.findForward(MODIFY_FAIL);

                    }

                } else if (!"".equals(newname)) {

                    scriptpath.trim();
                    i = scriptpath.lastIndexOf("/");
                    j = scriptpath.length();
                    onlyscriptname = scriptpath.substring(i + 1, j);
                    System.out.println("--------------------------------------------------------------------------------");
                    System.out.println("I am the actual name of script from scriptpath as calculated by modify script action" + onlyscriptname);
                    System.out.println("--------------------------------------------------------------------------------");

                    pathwidnewname = scriptpath.replace(onlyscriptname, newname);

                    if (refssh.editFile(hostname, username, password, editedscript.trim(), pathwidnewname.trim(),homedir)) {

                        System.out.println("In modify script action: Script Modification Successful With Name Change");
                        session.setAttribute("action_stat", "yes");
                        return mapping.findForward(MODIFY_SUCCESS);
                    } else {

                        session.setAttribute("action_stat", "no");
                        return mapping.findForward(MODIFY_FAIL);

                    }

                }
            }
        } catch (Exception ex) {

            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside Exception Block of Modify--Script--Action");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println(ex.getStackTrace());
            session.setAttribute("action_stat", "ex");
            return mapping.findForward(MODIFY_FAIL);
        }
        session.setAttribute("action_stat", "yes");
        return mapping.findForward(MODIFY_SUCCESS);

    }
}
