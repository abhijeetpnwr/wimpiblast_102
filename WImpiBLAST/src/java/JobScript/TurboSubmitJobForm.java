/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JobScript;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author parichit
 */
public class TurboSubmitJobForm extends org.apache.struts.action.ActionForm {
    
    private String inputfilepath = "";
    private String blastbinary = "";
    private String blastdb = "";
    private FormFile upFile = null;
    private String evalue = "";
    private String outfmt = "";
    private String numdesc = "";
    private String numalgn = "";
    private String emailid = "";

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }
    
    public FormFile getUpFile() {
        return upFile;
    }

    public void setUpFile(FormFile upFile) {
        this.upFile = upFile;
    }
    
    
    public String getInputfilepath() {
        return inputfilepath;
    }

    public void setInputfilepath(String inputfilepath) {
        this.inputfilepath = inputfilepath;
    }

    public String getBlastbinary() {
        return blastbinary;
    }

    public void setBlastbinary(String blastbinary) {
        this.blastbinary = blastbinary;
    }

    public String getBlastdb() {
        return blastdb;
    }

    public void setBlastdb(String blastdb) {
        this.blastdb = blastdb;
    }

    public String getEvalue() {
        return evalue;
    }

    public void setEvalue(String evalue) {
        this.evalue = evalue;
    }

    public String getOutfmt() {
        return outfmt;
    }

    public void setOutfmt(String outfmt) {
        this.outfmt = outfmt;
    }

    public String getNumdesc() {
        return numdesc;
    }

    public void setNumdesc(String numdesc) {
        this.numdesc = numdesc;
    }

    public String getNumalgn() {
        return numalgn;
    }

    public void setNumalgn(String numalgn) {
        this.numalgn = numalgn;
    }

    
    
}
