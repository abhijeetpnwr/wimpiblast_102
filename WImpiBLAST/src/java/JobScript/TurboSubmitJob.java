/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JobScript;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import logic_core.Core_func;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author parichit
 */
public class TurboSubmitJob extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String TURBO_SUCCESS = "success";
    private static final String TURBO_FAIL = "fail";

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        TurboSubmitJobForm editedform = (TurboSubmitJobForm) form;
        FormFile localfilename = editedform.getUpFile();
        String inputfile = editedform.getInputfilepath();
        String blastbinary = editedform.getBlastbinary();
        String blastdb = editedform.getBlastdb();
        String evalue = editedform.getEvalue();
        String outfmt = editedform.getOutfmt();
        String numdesc = editedform.getNumdesc();
        String numalgn = editedform.getNumalgn();
        String emailid = editedform.getEmailid();
        String mpipath = "empty";
        String mpiblastpath = "empty";
        BigDecimal op1;
        BigDecimal op2;
        BigDecimal np = null;
        String strLine = "";
        Core_func refssh = Core_func.getInstance();
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();
        String homedir = session.getAttribute("homedir").toString();
        String Status = null;
        String[] result;
        String jobid = null;
        String inputpath = homedir + "WIMPIHome/" + inputfile + "_TurboJob/" + inputfile;
        String outputpath = homedir + "WIMPIHome/" + inputfile + "_TurboJob/" + inputfile + "_turbo_result";
        String scriptloc = homedir + "WIMPIHome/" + inputfile + "_TurboJob/" + "/" + inputfile + "_turbo_script.sh";
        String jobfolder = homedir + "WIMPIHome/" + inputfile + "_TurboJob/";

        try {

            Status = refssh.CreateTurboFile(hostname, username, password, inputfile, emailid, homedir);
            if (Status.equals("FileCreationConversionSuccess") | Status.equals("FileCreationSuccessConversionFail")) {
                System.out.println("File created successfully");
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                System.out.println("Inside Turbo job action class, scriptname" + localfilename);
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                //return mapping.findForward(TURBO_SUCCESS);
            } else if (Status.equals("JobFldrExists")) {
                session.setAttribute("action_stat", "fldrexst");
                return mapping.findForward(TURBO_FAIL);

            } else if (Status.equals("FldrCreationExcep")) {
                session.setAttribute("action_stat", "fldrexcep");
                return mapping.findForward(TURBO_FAIL);

            } else if (Status.equals("dataappendingexception")) {
                session.setAttribute("action_stat", "dtaexcep");
                return mapping.findForward(TURBO_FAIL);

            } else if (Status.equals("FileExists")) {
                session.setAttribute("action_stat", "fileexst");
                return mapping.findForward(TURBO_FAIL);

            } else if (Status.equals("PermissionConflicts")) {
                session.setAttribute("action_stat", "prmcnflct");
                return mapping.findForward(TURBO_FAIL);


            }
            File conf_file = new File("/etc/.WImpiBLAST.conf");
            if (conf_file.getAbsoluteFile().exists()) {
                FileInputStream finputstream = new FileInputStream("/etc/.WImpiBLAST.conf");
                BufferedReader br = new BufferedReader(new InputStreamReader(finputstream));
                System.out.println("mpirun path before calculation is : " + mpipath);
                System.out.println("mpiblast path before calculation is : " + mpiblastpath);
                if (mpiblastpath.equals("empty") || mpipath.equals("empty")) {
                    while ((strLine = br.readLine()) != null) {

                        if (strLine.startsWith("mpirun_path")) {
                            int len = strLine.length();
                            mpipath = strLine.substring(13, len - 1).toString().trim();

                            if (mpipath.equals("") || mpipath.isEmpty()) {
                                session.setAttribute("action_stat", "param_fail");
                                return mapping.findForward(TURBO_FAIL);
                            }

                        } else if (strLine.startsWith("mpiblast_path")) {

                            int len = strLine.length();
                            mpiblastpath = strLine.substring(15, len - 1).toString().trim();

                            if (mpiblastpath.equals("") || mpiblastpath.isEmpty()) {
                                session.setAttribute("action_stat", "param_fail");
                                return mapping.findForward(TURBO_FAIL);
                            }

                        }

                    }

                    System.out.println("------------------------------------------------------------------\n");
                    System.out.println("mpirun path after calculation is : " + mpipath);
                    System.out.println("------------------------------------------------------------------\n");
                    System.out.println("mpiblast path after calculation is : " + mpiblastpath);
                    System.out.println("------------------------------------------------------------------\n");

                }

            } else {
                System.out.println("------------------------------------------------------------------\n");
                System.out.println("WImpiBLAST conf file undetected in mturbo job action class file , so returning false");
                System.out.println("------------------------------------------------------------------\n");
                session.setAttribute("action_stat", "param_fail");
                return mapping.findForward(TURBO_FAIL);

            }

            System.out.println("-------------------------------");
            System.out.println("location of upload folder on server is : " + jobfolder);
            System.out.println("-------------------------------");
            String filename = localfilename.getFileName();
            System.out.println("-------------------------------");
            System.out.println("Just before calling upload function of logic_core.java from turbo job action file");
            System.out.println("-------------------------------");

            if (filename.contains(" ")) {
                filename = filename.replace(" ", "_");
            }

            if (refssh.fileUpload(hostname, username, password, filename, jobfolder, localfilename, homedir, "appliupld")) {
                session.setAttribute("action_stat", "yesupld");
                //session.setAttribute("ippath", homedir +"WIMPIHome/"+ jobfolder + "/" + localfilename);
                //return mapping.findForward(TURBO_SUCCESS);

            } else {
                session.setAttribute("action_stat", "noupld");
                session.removeAttribute("ip_path");
                return mapping.findForward(TURBO_FAIL);

            }

            System.out.println("From turbo job action, scriptname: " + inputfile);
            op1 = new BigDecimal("1");
            op2 = new BigDecimal("24");
            np = op1.multiply(op2);
            //System.out.println(np);


            if (refssh.maketurboblastdata(hostname, username, password, mpipath, np, mpiblastpath, blastbinary, blastdb, inputpath, evalue, outfmt, numalgn, numdesc, outputpath, scriptloc)) {
                System.out.println("------------------------------------------------------------------\n");
                System.out.println("Appending of mpiblast specific data to script is successfule\n");
                System.out.println("------------------------------------------------------------------\n");
                session.removeAttribute("nnodes");
                session.removeAttribute("ppn");
                //session.removeAttribute("jobname");
                result = refssh.quicksubmit(hostname, username, password, jobfolder + inputfile + "_turbo_script.sh");
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                //System.out.println("the result length after executing submitjob function is " + result.length + " " + result[0] + " " + result[1]);
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                if (result.length != 2) {

                    System.out.println("------------------------------------------");
                    System.out.println("Job Submission is unsucessful");
                    System.out.println("------------------------------------------");
                    session.setAttribute("action_stat", "jobfail");
                    return mapping.findForward(TURBO_FAIL);
                } else {
                    if (result.length == 2) {

                        if (result[1].equals("0")) {
                            jobid = result[0];
                            System.out.println("----------------------------------------------------");
                            System.out.println("The Job submission successful with jobid!!!" + jobid);
                            System.out.println("----------------------------------------------------");
                            session.setAttribute("action_stat", "jobsuccess");
                            session.removeAttribute("jobname");
                            session.removeAttribute("scriptname");
                            scriptloc = null;
                            //return mapping.findForward(TURBO_SUCCESS);

                        } else {
                            System.out.println("-------------------------------------------");
                            System.out.println("The Job submission unsuccessful !!!");
                            System.out.println("-------------------------------------------");
                            session.setAttribute("action_stat", "jobfail");
                            return mapping.findForward(TURBO_FAIL);
                        }

                    }
                }


            } else {
                System.out.println("------------------------------------------------------------------\n");
                System.out.println("Appending of mpiblast data at the end of file is unsuccessful\n");
                System.out.println("------------------------------------------------------------------\n");
                session.removeAttribute("scriptname");
                session.removeAttribute("nnodes");
                session.removeAttribute("ppn");
                session.removeAttribute("jobname");
                session.setAttribute("action_stat", "no");
                return mapping.findForward(TURBO_FAIL);

            }

        } catch (Exception exc) {
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside exception of Turbo Action Class");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            session.setAttribute("action_stat", "ex");
            return mapping.findForward(TURBO_FAIL);


        }

        return mapping.findForward(TURBO_SUCCESS);
    }
}
