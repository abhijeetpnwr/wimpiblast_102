/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file perform multiple functions of file downloading and directory downloading when user
 * selects either file or directory and presses on 'Download' button, a call is sent to this file
 * and it downloads that file onto client's system. There are also validations on size of file and 
 * directory that can be downloaded.
 */


package Utility;

import logic_core.Core_func;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.io.FileUtils;

public class DownloadAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String DWNLD_SUCCESS = "success";
    private static final String DWNLD_FAIL = "fail";

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        DownloadForm myform = (DownloadForm) form;
        String current = myform.getCurdir();
        String directory = myform.getDirectory();
        String button = myform.getButton();
        String newfolder = myform.getNewfolder();
        String changedir = current;
        String tree = myform.getTree();
        String file_to_be_downloaded = myform.getFile_to_be_downloaded();
        String key_for_directory = myform.getUnique_key_for_directory();
        String key_for_files = myform.getUnique_key_for_files();
        String file_or_directory = myform.getFiles_or_directories();
        HttpSession session = request.getSession();
        Core_func refssh = Core_func.getInstance();

        String username = session.getAttribute("username").toString();
        String hostip = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();
        ArrayList<String> convertpath = new ArrayList<String>();

        StringBuilder sb = new StringBuilder();
        File file = null ;
        String real_path_crude = null;
        String replace = null;
        String cmd = null;
        FileInputStream in = null;
        ServletOutputStream out = null;
        FileInputStream in1 = null;
        byte[] outputByte;

        System.out.println("Button Value:" + button);
 
        if (button.equals("Download")) {
            if (file_or_directory.equals("files")) {
                real_path_crude = refssh.calculaterealpath(hostip, username, password, key_for_files);

                try {
                    in = new FileInputStream(new File(real_path_crude.toString().trim()));
                    int size = (int) in.getChannel().size();

                    if (size <= 104857600) {
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=" + file_to_be_downloaded);
                        out = response.getOutputStream();
                        outputByte = new byte[size];
                        //copy binary content to output stream
                        while (in.read(outputByte, 0, size) != -1) {
                            out.write(outputByte, 0, size);
                        }
                        in.close();
                        out.flush();
                        out.close();

                        return mapping.findForward(DWNLD_SUCCESS);

                    } else if (size > 104857600) {
                        session.setAttribute("action_stat", "nodwn");
                        return mapping.findForward(DWNLD_FAIL);
                    }

                } catch (Exception e) {

                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                    System.out.println("Inside exception code of file download code of download action class");
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                    session.setAttribute("action_stat", "excepdwn");
                    return mapping.findForward(DWNLD_FAIL);
                }


            } else if (file_or_directory.equals("directory")) {

                real_path_crude = refssh.calculaterealpath(hostip, username, password, key_for_directory);

                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                System.out.println("The real path to directory is: " + real_path_crude);
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

                File dir_to_dnld = new File(real_path_crude.trim());
                File comp_dir_to_dnld = new File(real_path_crude.trim()+".tar");
                //System.out.println(dir_to_dnld+" to be downloaded");

                long size = FileUtils.sizeOfDirectory(dir_to_dnld);

                if (size <= 104857600) {

                    String split[] = real_path_crude.split("/");
                    for (int i = 0; i <= split.length - 2; i++) {
                        convertpath.add(split[i]);
                        System.out.println(convertpath.get(i));
                    }
                    convertpath.remove("");
                    for (String s : convertpath) {
                        sb.append("/");
                        sb.append(s);
                        // sb.append("/");
                    }

                    if (file_to_be_downloaded.contains(" ")) {
                        replace = file_to_be_downloaded.replace(" ", "\\ ");
                        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                        System.out.println("Here is the file without spaces" + replace);
                        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                        cmd = "cd " + sb.toString().trim() + "/" + " && tar -cvf " + replace.concat(".tar") + " " + replace + "; echo $?";

                    } else {

                        cmd = "cd " + sb.toString().trim() + "/" + " && tar -cvf " + file_to_be_downloaded + ".tar" + " " + file_to_be_downloaded + "/ ;echo $?";
                    }

                    String stat = refssh.executecommand(hostip, username, password, cmd);
                    if (stat.equals("true")) {
                        System.out.println("--------------------------------------------");
                        System.out.println("Inside downloading of converted Tar file");
                        System.out.println("--------------------------------------------");
                        response.setContentType("application/x-tar");
                        response.setHeader("Content-Disposition", "attachment;filename=" + file_to_be_downloaded.concat(".tar"));

                        try {

                            in1 = new FileInputStream(real_path_crude.trim() + ".tar");
                            //file = new File(real_path_crude+".tar");
                            int sizetar = (int) in1.getChannel().size();
                            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            System.out.println("This is the path to converted tar file: " + real_path_crude + ".tar");
                            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            out = response.getOutputStream();
                            outputByte = new byte[sizetar];
                            //copy binary content to output stream
                            while (in1.read(outputByte, 0, sizetar) != -1) {
                                out.write(outputByte, 0, sizetar);
                            }
                            in1.close();
                            out.flush();
                            out.close();
                            //String command = "cd " + sb.toString().trim() + "/" + " && rm -rf " + file_to_be_downloaded + ".tar ; echo $?";
                            //String status = refssh.executecommand(hostip, username, password, command);
                            if(comp_dir_to_dnld.delete()){
                            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            System.out.println("File deletion successful afer download");
                            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            return mapping.findForward(DWNLD_SUCCESS);
                            }

                        } catch (Exception e) {
                            System.out.println(e.toString() + "this is directory download error");
                            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                            System.out.println("Inside exceptionn code of tar conversion of download action class");
                            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                            //String command = "cd " + sb.toString().trim() + "/" + " && rm -rf " + file_to_be_downloaded + ".tar ; echo $?";
                           // String status = refssh.executecommand(hostip, username, password, command);
                            if(comp_dir_to_dnld.delete()){
                            System.out.println("-------------------------------------------------");
                            System.out.println("Deleted tar file due to exception in converison process");
                            System.out.println("-------------------------------------------------");
                            session.setAttribute("action_stat", "excepdwn");
                            return mapping.findForward(DWNLD_FAIL);
                            }
                        }

                    } else {

                        System.out.println("--------------------------------------------");
                        System.out.println("tar was not made successfully !!!");
                        System.out.println("--------------------------------------------");
                        session.setAttribute("action_stat", "nodwn");
                        return mapping.findForward(DWNLD_FAIL);

                    }
                } else if (size > 104857600) {
                    session.setAttribute("action_stat", "nodwn");
                    System.out.println("------------------------------------------------------------------------------------------------------------");
                    System.out.println("Inside else block of directory size check of download action class, convetred tar file crosses maximum limit");
                    System.out.println("------------------------------------------------------------------------------------------------------------");
                    //String command = "cd " + sb.toString().trim() + "/" + " && rm -rf " + file_to_be_downloaded + ".tar ; echo $?";
                    //String status = refssh.executecommand(hostip, username, password, command);
                    if(comp_dir_to_dnld.delete()){
                    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                    System.out.println("Deleted tar file due to size violation");
                    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                    return mapping.findForward(DWNLD_FAIL);
                    }
                }

            }
        }
        return mapping.findForward(DWNLD_SUCCESS);
    }
}