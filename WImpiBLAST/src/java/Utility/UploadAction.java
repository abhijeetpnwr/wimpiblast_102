/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file does the job of uploading of files form client's machine to server home directory of user.
 * A directory in the name of 'upload' is created in home directory of user inside which the uploaded 
 * files will be copied.
 */
package Utility;

import logic_core.Core_func;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author parichit
 */
public class UploadAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String UPLD_SUCCESS = "success";
    private static final String UPLD_FAIL = "fail";

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        UploadForm myform = (UploadForm) form;
        HttpSession session = request.getSession();
        Core_func refssh = Core_func.getInstance();
        FormFile localfilename = myform.getUploadFile();
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        System.out.println("file to be uploaded is :" + localfilename);
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();

        try {
            String homedir = session.getAttribute("homedir").toString();
            String pathfrupload = homedir + "WIMPIUpload";
            System.out.println("-------------------------------");
            System.out.println("location of upload folder on server is : " + pathfrupload);
            System.out.println("-------------------------------");
            String filename = localfilename.getFileName();
            System.out.println("-------------------------------");
            System.out.println("Just before calling upload function of SSH.java from UploadAction.java file");
            System.out.println("-------------------------------");
            if (refssh.fileUpload(hostname, username, password, filename, pathfrupload, localfilename, homedir,"util")) {
                session.setAttribute("action_stat", "yesupld");

                return mapping.findForward(UPLD_SUCCESS);


            } else {
                session.setAttribute("action_stat", "noupld");

                return mapping.findForward(UPLD_FAIL);

            }

        } catch (Exception e) {
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside Exception block of Upload action class file");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            session.setAttribute("action_stat", "excepupld");
            e.printStackTrace();
            return mapping.findForward(UPLD_FAIL);

        }

    }
}
