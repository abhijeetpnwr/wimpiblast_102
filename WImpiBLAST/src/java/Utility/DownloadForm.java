/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import org.apache.struts.upload.FormFile;

public class DownloadForm extends org.apache.struts.action.ActionForm {
    
    private String curdir = "";
    private String directory = "";
    private String button = "";
    private String newfolder = "";
    private String tree = "";
    private String file = "";
    private String dirfilesel;
    private FormFile uploadFile = null;
    private String file_to_be_downloaded = "";
    private String unique_key_for_files = "";
    private String unique_key_for_directory = "" ;
    private String files_or_directories = "";

    public String getFiles_or_directories() {
        return files_or_directories;
    }

    public void setFiles_or_directories(String files_or_directories) {
        this.files_or_directories = files_or_directories;
    }

    public String getUnique_key_for_files() {
        return unique_key_for_files;
    }

    public void setUnique_key_for_files(String unique_key_for_files) {
        this.unique_key_for_files = unique_key_for_files;
    }

    public String getUnique_key_for_directory() {
        return unique_key_for_directory;
    }

    public void setUnique_key_for_directory(String unique_key_for_directory) {
        this.unique_key_for_directory = unique_key_for_directory;
    }

    public String getFile_to_be_downloaded() {
        return file_to_be_downloaded;
    }

    public void setFile_to_be_downloaded(String file_to_be_downloaded) {
        this.file_to_be_downloaded = file_to_be_downloaded;
    }

    
    

    public FormFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(FormFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getDirfilesel() {
        return dirfilesel;
    }

    public void setDirfilesel(String dirfilesel) {
        this.dirfilesel = dirfilesel;
    }

    public String getCurdir() {
        return curdir;
    }

    public void setCurdir(String curdir) {
        this.curdir = curdir;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getNewfolder() {
        return newfolder;
    }

    public void setNewfolder(String newfolder) {
        this.newfolder = newfolder;
    }

    public String getTree() {
        return tree;
    }

    public void setTree(String tree) {
        this.tree = tree;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
   
}
