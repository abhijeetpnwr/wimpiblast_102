/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file performs action of submitting the job when user clicks on submit job button. It also reports
 *user about status of job submision based on result returned. 
 * 
 */

package JobManagement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import logic_core.Core_func;

/**
 *
 * @author parichit
 */
public class SubmitJobAction extends org.apache.struts.action.Action {

    private static final String SUBMIT_SUCCESS = "success";
    private static final String SUBMIT_FAIL = "fail";

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        SubmitJobForm editedform = (SubmitJobForm) form;

        String scriptpath = editedform.getScriptpath();
        String scriptpath1 = editedform.getScriptpath1();
        String jobname = editedform.getJobname();
        String ppn = editedform.getPpn();
        String nnodes = editedform.getNnodes();
        String wtime = editedform.getWtime();
        String start = editedform.getStart();
        String end = editedform.getEnd();
        String abort = editedform.getAbort();
        String button = editedform.getButton();
        String emailid = editedform.getEmailid();
        String errorloc = editedform.getErrorloc();
        String oploc = editedform.getOutputloc();
        String randomscript = editedform.getRandomscript();
        Core_func refssh = Core_func.getInstance();
        String[] result;
        String jobid = null;
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();


        System.out.println("script path is" + scriptpath1);
        System.out.println("button value is "+button);

       try{
        
        if (button.equals("SUBMIT")) {
            
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            System.out.println("script path is" + scriptpath1);
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
  
            result = refssh.submitjob(hostname, username, password, scriptpath, jobname, wtime, nnodes, ppn, oploc, errorloc, start, end, abort, emailid, scriptpath1, randomscript);
            
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            System.out.println("the result length after executing submitjob function is " + result.length + " " + result[0] + " " + result[1]);
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            if (result.length != 2) {
                
                System.out.println("------------------------------------------");
                System.out.println("Job Submission is unsucessful");
                System.out.println("------------------------------------------");
                session.setAttribute("action_stat", "no");
                return mapping.findForward(SUBMIT_FAIL);

            } else {
                if (result.length == 2) {
                    
                    if (result[1].equals("0")) {
                        jobid = result[0];
                        System.out.println("----------------------------------------------------");
                        System.out.println("The Job submission successful with jobid!!!" + jobid);
                        System.out.println("----------------------------------------------------");
                        session.setAttribute("action_stat", "yes");

                    } else {
                        System.out.println("-------------------------------------------");
                        System.out.println("The Job submission unsuccessful !!!");
                        System.out.println("-------------------------------------------");
                        session.setAttribute("action_stat", "no");
                        return mapping.findForward(SUBMIT_FAIL);
                    }
                  
                }
            }
  
        }
       }
       catch(Exception e){
           System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
           System.out.println("Inside exception of Submit--Job--action--class");
           System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
           session.setAttribute("action_stat", "ex");
           e.printStackTrace();
           return mapping.findForward(SUBMIT_FAIL);
       }
       return mapping.findForward(SUBMIT_SUCCESS); 
    }
}
