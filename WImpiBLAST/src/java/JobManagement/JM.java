/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * This file is equivalent to takes command from global param file and execute those commands for  
 * user specific jsp files. This file is for user what Adm_JM.java is for administrator.
 */

package JobManagement;

import GlobalParam.GlobalParam;
import static GlobalParam.GlobalParam.JOB_ELAP_TIME1;
import static GlobalParam.GlobalParam.JOB_ELAP_TIME2;
import static GlobalParam.GlobalParam.JOB_NODE_NAME1;
import static GlobalParam.GlobalParam.JOB_NODE_NAME2;
import static GlobalParam.GlobalParam.JOB_STATE1;
import static GlobalParam.GlobalParam.JOB_STATE2;
import logic_core.Core_func;

public class JM implements GlobalParam {

    Core_func sref = Core_func.getInstance();
    private static JM ref = null;

    public static JM getInstance() {
        if (ref == null) {
            try {
                ref = new JM();
            } catch (Exception e) {
                ref = null;
            }
        }
        return ref;
    }

    public synchronized String[] scriptname(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -f |grep 'submit_args'| awk -F' ' '{print $NF}' ");
    }
    
    public synchronized String[] jobID_short(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+userName+" |awk '{print$1}'| sed -n '6,$p'");
    }

    public synchronized String[] jobID(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+userName+" |awk '{print$1}'| sed -n '6,$p'");
    }

    public synchronized String[] uName_short(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+userName+"|awk '{print$2}'| sed '1,5d'");
    }

    public synchronized String[] uName(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOB_UNAME_short1+userName+JOB_UNAME_short2);
    }

    public synchronized String[] jobQueue_short(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+userName+"| awk '{print$3}'| sed '1,5d'");
    }

    public synchronized String[] jobQueue(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOBQUEUE_short1+userName+JOBQUEUE_short2);
    }

    public synchronized String[] jName_short(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+userName+" |awk '{print$4}'| sed '1,5d'");

    }

    public synchronized String[] jName(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOBNAME_short1+userName+JOBNAME_short2);

    }
    
    public synchronized String[] timeReq(String hostName, String userName,String password)
    {
        return sref.executecmd(hostName, userName,password,JOB_TIMEREQD1+userName+JOB_TIMEREQD2);
    }
  

    public synchronized String[] Nodes(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOB_NODES1+userName+JOB_NODES2);
    }

    public synchronized String[] ppn(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOB_PPN1+userName+JOB_PPN2);
    }
    
    public synchronized String[] memReq(String hostName, String userName,String password)
    {
        return sref.executecmd(hostName, userName,password,JOB_MEMREQD1+userName+JOB_MEMREQD2);
    }
    

    public synchronized String[] state_short(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+userName+" |awk '{print$10}'| sed '1,5d'");
    }

    public synchronized String[] state(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOB_STATE1+userName+JOB_STATE2);
    }

    public synchronized String[] elaspedTime(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOB_ELAP_TIME1+userName+JOB_ELAP_TIME2);
    }

    public synchronized String[] nodename(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, JOB_NODE_NAME1+userName+JOB_NODE_NAME2);
    }

    public synchronized String[] jobID_fltr(String hostName, String userName, String password, String slctduser) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+slctduser+"| awk '{print$1}'| sed -n '6,$p'");
    }
    public synchronized String[] uName_short_fltr(String hostName, String userName, String password, String slctduser) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+slctduser+"| awk '{print$2}'| sed '1,2d' |sed -n '1,3!p'");
    }

    public synchronized String[] jobQueue_short_fltr(String hostName, String userName, String password, String slctduser) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+slctduser+"| awk '{print$3}'| sed '1,2d'|sed -n '1,3!p'");
    }

    public synchronized String[] jName_short_fltr(String hostName, String userName, String password, String slctduser) {
        return sref.executecmd(hostName, userName, password, "qstat -u "+slctduser+"| awk '{print$4}'| sed '1,2d'|sed -n '1,3!p'");

    }
    
    public synchronized String[] numqueues(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, numqueues);

    }
    
    public synchronized String[] queueName(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, queueName);

    }
    
    public synchronized String[] totRunJob(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, totRunJob);

    }
    
    public synchronized String[] queStrted(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, queStrted);

    }
    
    public synchronized String[] queEnbld(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, queEnbld);

    }
    
    public synchronized String[] queType(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, queType);

    }
    
    public synchronized String[] jobHld(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, jobHld);

    }
    
    public synchronized String[] jobRun(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, jobRun);

    }
    
    public synchronized String[] jobQued(String hostName, String userName, String password) {
        return sref.executecmd(hostName, userName, password, jobQued);

    }

    public synchronized String[] state_short_fltr(String hostName, String userName, String password, String slctduser) {
        return sref.executecmdfltr(hostName, userName, password, "qstat -u "+slctduser+"| awk '{print$10}'| sed '1,2d'|sed -n '1,3!p'");
    }

   
    public synchronized String jobdone(String hostName, String userName, String password, String jobcmd, Core_func sshref) {
        return sshref.executecmdstring(hostName, userName, password, jobcmd);
    }
}
