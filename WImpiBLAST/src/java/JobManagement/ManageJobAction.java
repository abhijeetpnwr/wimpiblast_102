/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file performs the function of job deletion , hold and release based on user input.
 * 
 */


package JobManagement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import logic_core.Core_func;
import javax.servlet.http.HttpSession;

public class ManageJobAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String MANAGE_SUCCESS = "success";
    private static final String MANAGE_FAIL = "fail";

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ManageJobForm editedform = (ManageJobForm) form;
        String[] jobid = editedform.getJobid();
        String button = editedform.getButton();
        StringBuilder jobs = new StringBuilder();
        HttpSession session = request.getSession();
        int job = 0;
        JM jm = JM.getInstance();
        String hostname = (String) session.getAttribute("hostip");
        String username = (String) session.getAttribute("username");
        String passWd = (String) session.getAttribute("password");
        Core_func sshref = Core_func.getInstance();


        try {

            if (button.equals("DELETE")) {
                jobs.append("qdel ");
                for (int i = 0; i < jobid.length; i++) {
                    job = jobid[i].indexOf(".");
                    jobid[i] = jobid[i].substring(0, job);
                    jobs.append(jobid[i]).append(" ");
                }
                System.out.println("Job Delete :" + jobs.toString());
                jm.jobdone(hostname, username, passWd, jobs.toString(), sshref);
            }

            if (button.equals("HOLD")) {
                jobs.append("qhold ");
                for (int i = 0; i < jobid.length; i++) {
                    job = jobid[i].indexOf(".");
                    jobid[i] = jobid[i].substring(0, job);
                    jobs.append(jobid[i]).append(" ");
                }
                System.out.println("Job Hold :" + jobs.toString());
                jm.jobdone(hostname, username, passWd, jobs.toString() + " | echo $?", sshref);
            }

            if (button.equals("RELEASE")) {
                jobs.append("qrls ");
                for (int i = 0; i < jobid.length; i++) {
                    job = jobid[i].indexOf(".");
                    jobid[i] = jobid[i].substring(0, job);
                    jobs.append(jobid[i]).append(" ");
                }
                System.out.println("Job Release :" + jobs.toString());
                jm.jobdone(hostname, username, passWd, jobs.toString() + " | echo $?", sshref);

            }

            editedform.setButton("");
            return mapping.findForward(MANAGE_SUCCESS);

        } catch (Exception e) {

            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside exception block of manage--job--action");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            //session.setAttribute("mnge_job", "ex");
            return mapping.findForward(MANAGE_FAIL);

        }


    }
}
