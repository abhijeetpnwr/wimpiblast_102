/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JobManagement;

/**
 *
 * @author parichit
 */
public class SubmitJobForm extends org.apache.struts.action.ActionForm {
    
   
        private String scriptpath="" ;
        private String jobname="" ;
        private String wtime="" ;
        private String nnodes="" ;
        private String ppn="" ;
        private String outputloc="" ;
        private String errorloc="" ;
        private String start="" ;
        private String end="" ;
        private String abort="" ;
        private String emailid="" ;
        private String button="" ;
        private String scriptpath1="";
        private String randomscript="";

    public String getRandomscript() {
        return randomscript;
    }

    public void setRandomscript(String randomscript) {
        this.randomscript = randomscript;
    }

    public String getScriptpath() {
        return scriptpath;
    }

    public void setScriptpath(String scriptpath) {
        this.scriptpath = scriptpath;
    }

    public String getJobname() {
        return jobname;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public String getWtime() {
        return wtime;
    }

    public void setWtime(String wtime) {
        this.wtime = wtime;
    }

    public String getPpn() {
        return ppn;
    }

    public void setPpn(String ppn) {
        this.ppn = ppn;
    }

    public String getOutputloc() {
        return outputloc;
    }

    public void setOutputloc(String outputloc) {
        this.outputloc = outputloc;
    }

    public String getErrorloc() {
        return errorloc;
    }

    public void setErrorloc(String errorloc) {
        this.errorloc = errorloc;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getAbort() {
        return abort;
    }

    public void setAbort(String abort) {
        this.abort = abort;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getScriptpath1() {
        return scriptpath1;
    }

    public void setScriptpath1(String scriptpath1) {
        this.scriptpath1 = scriptpath1;
    }
 
     public String getNnodes() {
        return nnodes;
    }

    public void setNnodes(String nnodes) {
        this.nnodes = nnodes;
    }
}
