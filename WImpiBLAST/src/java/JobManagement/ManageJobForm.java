/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JobManagement;

/**
 *
 * @author parichit
 */
public class ManageJobForm extends org.apache.struts.action.ActionForm {
    
    private String []jobid;
    private String []state;
    private String button;
    static final long serialVersionUID = 42L;
    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String[] getJobid() {
        return jobid;
    }

    public void setJobid(String[] jobid) {
        this.jobid = jobid;
    }

    public String[] getState() {
        return state;
    }

    public void setState(String[] state) {
        this.state = state;
    }
 
    public ManageJobForm() {
        super();
        // TODO Auto-generated constructor stub
    }

  
}
