/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file does the job of checking validity of authentic user and allowing them to use
 * WImpiBLASt interface. It also checks for presence of configuration file to fetch necessary  
 * parameters to perform login.
 * 
 */
package login;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.UserPrincipal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logic_core.Core_func;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author parichit
 */
public class LoginAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String LOGIN_SUCCESS = "success";
    private static final String LOGIN_FAIL = "fail";
    private static final String PARAM_FAIL = "param_fail";
    private static final String CONFIG_FAIL = "config_file_fail";

    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {

        LoginActionForm myform = (LoginActionForm) form;
        String hostip = "empty";
        String strLine = "empty";
        String username = myform.getUsername();
        String password = myform.getPassword();
        String homedir = "empty";
        String hostnewname = "empty";
        File wimpi_dir = null;
        Core_func refssh = Core_func.getInstance();
        HttpSession session = request.getSession();

        try {
            File conf_file = new File("/etc/.WImpiBLAST.conf");

            if (conf_file.getAbsoluteFile().exists()) {

                FileInputStream finputstream = new FileInputStream("/etc/.WImpiBLAST.conf");
                BufferedReader br = new BufferedReader(new InputStreamReader(finputstream));
                System.out.println("------------------------------------------------------------------\n");
                System.out.println("host ip before calculation is : " + hostip);
                System.out.println("------------------------------------------------------------------\n");

                while ((strLine = br.readLine()) != null) {

                    if (strLine.startsWith("hostip")) {
                        int len = strLine.length();
                        hostip = strLine.substring(8, len - 1).toString().trim();
                        if (hostip == null || hostip.isEmpty()) {
                            return mapping.findForward(PARAM_FAIL);
                        }
                    }

                }
                System.out.println("------------------------------------------------------------------\n");
                System.out.println("host ip as calculated from login action is : " + hostip);
                System.out.println("------------------------------------------------------------------\n");
                finputstream.close();


            } else {

                System.out.println("------------------------------------------------------------------\n");
                System.out.println(" configuration file undetected in login action class file , so returning false");
                System.out.println("------------------------------------------------------------------\n");
                return mapping.findForward(CONFIG_FAIL);
            }

            if (refssh.isValid(hostip, username, password)) {

                homedir = refssh.getHomeDir(hostip, username, password);
                session.setAttribute("hostip", hostip);
                session.setAttribute("username", username);
                session.setAttribute("password", password);
                session.setAttribute("homedir", homedir);
                hostnewname = refssh.getHostname(hostip, username, password);
                session.setAttribute("hostnewname", hostnewname);
                session.setAttribute("action_stat", "dflt");
                wimpi_dir = new File(homedir + "WIMPIHome");
                System.out.println("------------------------------------------------------------------\n");
                System.out.println("home directory from login action is" + homedir + "\n");
                System.out.println("------------------------------------------------------------------\n");

                
                System.out.println("------------------ Check line ------------");
                
                System.out.println("directory check value of"+username+" value of wimpi_dir"+wimpi_dir);
                
                System.out.println("boolean chk"+wimpi_dir.exists());
                
                System.out.println("user boolean chek"+(!username.equals("root")));
                
                if (!username.equals("root") && wimpi_dir.exists()) {
                    System.out.println("WImpiBLAST home already exists.");
                    return mapping.findForward(LOGIN_SUCCESS);
                } else{

                    System.out.println("WImpiBLAST home not exists, starting creation process");
                    File dfltfile = new File(homedir);
                    wimpi_dir.mkdir();
                    UserPrincipal owner;
                    owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                    Files.setOwner(wimpi_dir.toPath(), owner);
                    GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                    Files.getFileAttributeView(wimpi_dir.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                    wimpi_dir.setWritable(true);
                    wimpi_dir.setReadable(true);
                    System.out.println("\n\n-------------------------------\n\n");
                    System.out.println("WImpiBLAST home folder creation successful and permissions changed to default");
                    System.out.println("\n\n-------------------------------\n\n");
                    

                }
            } else {

                session.removeAttribute("hostip");
                session.removeAttribute("username");
                session.removeAttribute("password");
                session.removeAttribute("homedir");
                session.removeAttribute("action_stat");
                session.removeAttribute("hostnewname");
                return mapping.findForward(LOGIN_FAIL);


            }

        } catch (Exception e) {

            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside exception code of Login--Action--Class");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            session.removeAttribute("hostip");
            session.removeAttribute("username");
            session.removeAttribute("password");
            session.removeAttribute("homedir");
            session.removeAttribute("action_stat");
            session.removeAttribute("hostnewname");
            return mapping.findForward(LOGIN_FAIL);
        }
        
return mapping.findForward(LOGIN_SUCCESS);
    }
}
