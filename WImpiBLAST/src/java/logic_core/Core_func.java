/*51 cersion
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file is the heart of entire WImpiBLAST portal functionalities. All the functions and commands are
 * executed by this file and status is reported back to action files for reporting to user.
 * Where action files receives user requests they selects a specific function from this file to call 
 * to full fill user request. When that function completes execution the result data or status is
 * returned to action files for reporting to user.
 */
package logic_core;

import ch.ethz.ssh2.*;
import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.List;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author parichit
 */
public class Core_func {

    private static Core_func ref = null;
    Connection conn;
    Session sess = null;
    boolean isAuthenticated = false;

    public static Core_func getInstance() {
        if (ref == null) {
            try {
                ref = new Core_func();
            } catch (Exception e) {

                System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
                System.out.println("Error in geting instance of SSh " + e);
                System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
                ref = null;
            }
        }
        return ref;
    }

    public boolean authenticate(String hostName, String userName, String password) {
        try {
            conn = new Connection(hostName);
            conn.connect();
            isAuthenticated = conn.authenticateWithPassword(userName, password);
            if (isAuthenticated == false) {
                throw new IOException("Authentication failed.");
            }
            conn.close();
            conn = null;
            return true;
        } catch (IOException e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            System.out.println("Error in Authentication " + e);
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            return false;
        }
    }

    public boolean isValid(String hostName, String userName, String password) {
        System.out.println("In Isvalid");
        return ref.authenticate(hostName, userName, password);

    }

    public synchronized String[] executecmd(String hostName, String userName, String password, String command) {

        String[] myarray = null;
        try {
            conn = new Connection(hostName);
            conn.connect();
            isAuthenticated = conn.authenticateWithPassword(userName, password);
            if (isAuthenticated == false) {
                throw new IOException("Authentication failed.");
            }
            sess = conn.openSession();
            sess.execCommand(command);
            StringBuilder sb = new StringBuilder();
            InputStream stdout = new StreamGobbler(sess.getStdout());
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line);
                sb.append("\n");
            }

            myarray = sb.toString().split("\n");
            //System.out.println(sb.toString());
            stdout.close();
            br.close();
            sess.close();
            conn.close();
        } catch (Exception e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            System.out.println("Inside exception of runcmd");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            e.printStackTrace(System.err);
            //System.exit(2);
        }


        return myarray;

    }

    public synchronized String[] executecmdfltr(String hostName, String userName, String password, String command) {

        String[] myarray = null;
        System.out.println("command is " + command + "hostname is :" + hostName + "username is : " + userName);
        try {
            conn = new Connection(hostName);
            conn.connect();
            isAuthenticated = conn.authenticateWithPassword(userName, password);
            if (isAuthenticated == false) {
                throw new IOException("Authentication failed.");
            }
            sess = conn.openSession();
            sess.execCommand(command);
            StringBuilder sb = new StringBuilder();
            InputStream stdout = new StreamGobbler(sess.getStdout());
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
            String temp = sb.toString();
            myarray = temp.split("\n");
            stdout.close();
            br.close();
            sess.close();
            conn.close();
        } catch (Exception e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            System.out.println("Inside exception of runcmd");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            e.printStackTrace(System.err);
            //System.exit(2);
        }
        return myarray;

    }

    public synchronized String executecmdstring(String hostName, String userName, String password, String command) {
        String temp = "";
        try {
            conn = new Connection(hostName);
            conn.connect();
            isAuthenticated = conn.authenticateWithPassword(userName, password);
            if (isAuthenticated == false) {
                throw new IOException("Authentication failed.");
            }
            sess = conn.openSession();
            sess.execCommand(command);
            StringBuilder sb = new StringBuilder();
            InputStream stdout = new StreamGobbler(sess.getStdout());
            InputStream stderr = new StreamGobbler((sess.getStderr()));
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
            while (true) {
                String line = br.readLine();

                if (line == null) {
                    break;
                }
                //System.out.println("intermediate output is:" + line);
                sb.append(line);
                //sb.append(System.getProperty("line.separator"));
            }
            temp = sb.toString();
            System.out.println("\n\noutput from runcmdstring method is " + sb.toString() + "\n\n");
            stdout.close();
            br.close();
            sess.close();
            conn.close();
            sess = null;
            conn = null;
        } catch (IOException e) {

            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            System.out.println("Inside exception of runcmdstring method");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n ");
            e.printStackTrace(System.err);
            //System.exit(2);
            sess.close();
            conn.close();

        }
        //System.out.println("output from excuted command is" + temp);
        return temp;
    }

    public boolean closeAllconnection(String hostName, String userName, String password) {
        try {
            if (conn == null && sess == null) {
                return true;
            } else {
                sess.close();
                conn.close();
                System.out.println("the exit status" + sess.getExitStatus());
                sess = null;
                conn = null;
                return true;
            }
        } catch (Exception e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of closeallconnections method");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);
            return false;
        }
    }

    public boolean fileUpload(String hostName, String userName, String password, String filename, String pathfrupload, FormFile file, String homedir, String wchfrm) {

        try {
            File dfltfile = new File(homedir);
            File newFile = new File(pathfrupload, filename);

            System.out.println("\nInside upload function of logic_core\n");

            File folder = new File(pathfrupload);
            if (!folder.exists()) {
                folder.mkdir();
                UserPrincipal owner;
                owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                Files.setOwner(folder.toPath(), owner);
                GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                Files.getFileAttributeView(folder.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                folder.setWritable(true);
                folder.setReadable(true);
                System.out.println("\n\n-------------------------------\n\n");
                System.out.println("job folder created and permissions changed to default");
                System.out.println("\n\n-------------------------------\n\n");
            }

            if (!("").equals(filename)) {

                System.out.println("\n\n-------------------------------\n\n");
                System.out.println("Server path to upload files is:" + pathfrupload);
                System.out.println("\n\n-------------------------------\n\n");

                if (!newFile.exists()) {

                    FileOutputStream fos = new FileOutputStream(newFile);
                    fos.write(file.getFileData());
                    fos.flush();
                    fos.close();
                    UserPrincipal owner;
                    owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                    Files.setOwner(newFile.toPath(), owner);
                    GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                    Files.getFileAttributeView(newFile.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                    newFile.setWritable(true);
                    newFile.setReadable(true);
                    return true;
                } else {

                    if (wchfrm.equals("util")) {

                        System.out.println("\n\n-------------------------------\n\n");
                        System.out.println("File already exists hence cancelling upload operation!!!");
                        System.out.println("\n\n-------------------------------\n\n");
                        return false;

                    } else {
                        System.out.println("\n\n-------------------------------\n\n");
                        System.out.println("Uploaded File already exists in appliupld.");
                        System.out.println("\n\n-------------------------------\n\n");
                        return true;

                    }
                }

            } else {

                System.out.println("\n\n-------------------------------\n\n");
                System.out.println("Upload file name is empty hence canceling upload opeartion");
                System.out.println("\n\n-------------------------------\n\n");
                return false;

            }

        } catch (IOException e) {

            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of uploadfunction");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.err.println(e.toString());
            return false;

        }
    }

    public String calculaterealpath(String hostname, String username, String password, String inodevalue) throws IOException {


        String path_string = "";
        Connection conn1 = null;
        Session sess123 = null;
        String line_for_path;
        InputStream stdout_for_path = null;
        BufferedReader br_for_path = null;

        try {

            conn1 = new Connection(hostname);
            conn1.connect();
            isAuthenticated = conn1.authenticateWithPassword(username, password);
            if (isAuthenticated == false) {
                throw new IOException("Authentication failed.");
            }

            String homedir = getHomeDir(hostname, username, password);
            System.out.println("Local Home Directory Path is :" + homedir + inodevalue);
            sess123 = conn1.openSession();
            sess123.execCommand("find " + homedir + " -inum " + inodevalue);
            stdout_for_path = new StreamGobbler(sess123.getStdout());
            br_for_path = new BufferedReader(new InputStreamReader(stdout_for_path));

            while (true) {

                line_for_path = br_for_path.readLine();
                if (line_for_path == null) {
                    break;
                }
                path_string = line_for_path;
                System.out.println("Remote File Actual System Path is:" + path_string.trim());

            }


        } catch (IOException e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of calculate realpath");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace();
            conn1.close();
            stdout_for_path.close();
            br_for_path.close();
            path_string = "Sorry there is some problem. Please report this issue.";
            return path_string;

        }

        stdout_for_path.close();
        br_for_path.close();
        sess123.close();
        conn1.close();
        return path_string;
    }

    public String isRoot(String hostName, String userName, String password) {
        return ref.executecmdstring(hostName, userName, password, "id -u");
    }

    public String[] userList(String hostName, String userName, String password) {
        return ref.executecmd(hostName, userName, password, "cat /etc/passwd | grep '/home' | cut -d: -f1");
    }

    public String getHomeDir(String hostName, String userName, String password) {
        return ref.executecmdstring(hostName, userName, password, "echo $HOME/");
    }

    public String createnewFile(String hostName, String userName, String password, String scriptname, String jobname, String wtime, String nnodes, String ppn, String start, String end, String abort, String emailid, String homedir) {

        String check = null;
        StringBuilder input = new StringBuilder();
        System.out.println("\n\n In create file function, hostname is " + hostName + " filename is " + scriptname + " jobname is " + jobname + "\n");
        System.out.println("\n\nValue of start is " + start + " end is " + end + " abort is " + abort + " email id is " + emailid + "\n");
        File dfltfile = new File(homedir);
        File job_fldr = new File(homedir + "WIMPIHome/" + jobname);

        try {
            if (!job_fldr.exists()) {
                job_fldr.mkdir();
                UserPrincipal owner;
                owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                Files.setOwner(job_fldr.toPath(), owner);
                GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                Files.getFileAttributeView(job_fldr.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                job_fldr.setWritable(true);
                job_fldr.setReadable(true);
                System.out.println("\nJob folder creation successful and permissions changed to default");
            } else {

                System.out.println("\nJob folder already exist, hence returing false.");
                return "JobFldrExists";

            }

        } catch (Exception ex) {

            System.out.println("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Exception while creating job folder in create script function");
            System.out.println("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            return "FldrCreationExcep";

        }

        try {

            input.append("#!/bin/bash" + "\n");

            if (jobname.equals("")) {
            } else {
                input.append("#PBS -N ").append(jobname).append("\n");
            }

            if (wtime.equals("")) {
            } else {
                input.append("#PBS -l walltime=").append(wtime).append("\n");
            }

            if (nnodes.equals("") && ppn.equals("")) {
            } else {
                input.append("#PBS -l nodes=").append("nabi1.hpc.gpu1").append("\n");
            }

            input.append("#PBS -o ").append(job_fldr).append("/").append(scriptname).append(".out").append("_$PBS_JOBID").append(".txt"+" ").append("\n");

            input.append("#PBS -e ").append(job_fldr).append("/").append(scriptname).append(".err").append("_$PBS_JOBID").append(".txt"+" ").append("\n");

            if (start.equals("") || end.equals("") || abort.equals("")) {
            } else {
                input.append("#PBS -m ").append(start).append(end).append(abort).append("\n");
            }
            if (emailid.equals("")) {
                input.append("#PBS -M ").append("ict@nabi.res.in").append("\n");

            } else {
                input.append("#PBS -M ").append(emailid).append(",ict@nabi.res.in").append("\n");
            }
            System.out.println("The contents to be written to script are: " + input.toString());

        } catch (NullPointerException e) {

            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of createnewfile");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println(e);
            return "dataappendingexception";
        }
        try {


            File newFile = new File(job_fldr + "/" + scriptname);
            if (newFile.exists()) {
                System.out.println("File alreday exists.");
                return "FileExists";
            } else {
                File trgetfile = new File(job_fldr + "/" + scriptname);
                trgetfile.createNewFile();
                //UserPrincipal trget_owner;
                // trget_owner = Files.getOwner(trgetfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                UserPrincipal owner;
                owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                // if (trget_owner.equals(owner)) {
                BufferedWriter output = new BufferedWriter(new FileWriter(newFile));
                output.write(input.toString());
                output.close();
                Files.setOwner(newFile.toPath(), owner);
                GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                Files.getFileAttributeView(newFile.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                newFile.setWritable(true);
                newFile.setReadable(true);
                newFile.setExecutable(true);
                check = executecmdstring(hostName, userName, password, "dos2unix " + job_fldr + "/" + scriptname + ";echo $?");
                if (check.equals("0")) {

                    System.out.println("\\n---------------------------------------------------\n\n");
                    System.out.println("New script creation and DOS 2 UNIX conversion is successful");
                    System.out.println("\\n---------------------------------------------------\n\n");
                    return "FileCreationConversionSuccess";
                } else {
                    System.out.println("\\n---------------------------------------------------\n\n");
                    System.out.println("File creation is successful but DOS 2 UNIX conversion is unsuccessful");
                    System.out.println("\\n---------------------------------------------------\n\n");
                    return "FileCreationSuccessConversionFail";
                }
            } 


        } catch (Exception e) {
            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of create new file writing code");
            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);
            return "FileCreationException";
        }
    }

    public String CreateTurboFile(String hostName, String userName, String password, String scriptname, String emailid, String homedir) {

        String check = null;
        int j = 0;
        
        StringBuilder input = new StringBuilder();
        System.out.println("\n\n Inside create file function, hostname is " + hostName + " filename is " + scriptname);
        File dfltfile = new File(homedir);
        File job_fldr = new File(homedir + "WIMPIHome/" + scriptname + "_TurboJob");

        try {
            if (!job_fldr.exists()) {
                job_fldr.mkdir();
                UserPrincipal owner;
                owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                Files.setOwner(job_fldr.toPath(), owner);
                GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                Files.getFileAttributeView(job_fldr.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                job_fldr.setWritable(true);
                job_fldr.setReadable(true);
                System.out.println("\nJob folder creation successful and permissions changed to default");
            } else {

                System.out.println("\nJob folder already exist, hence returing false.");
                return "JobFldrExists";

            }


        } catch (Exception ex) {

            System.out.println("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Exception while creating job folder in create script function");
            System.out.println("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            return "FldrCreationExcep";
        }

        try {

            input.append("#!/bin/bash" + "\n");
            input.append("#PBS -N ").append(scriptname).append("_turboJob").append("\n");
            input.append("#PBS -l nodes=").append("nabi1.hpc.gpu1").append(":ppn=24").append("\n");
            input.append("#PBS -o ").append(job_fldr).append("/").append(scriptname).append(".Turbo.out").append("_$PBS_JOBID.txt" + " ").append("\n");
            input.append("#PBS -e ").append(job_fldr).append("/").append(scriptname).append(".Turbo.err").append("_$PBS_JOBID.txt" + " ").append("\n");
            input.append("#PBS -m ").append("a").append("b").append("e").append("\n");
            if (emailid.equals("")) {
                input.append("#PBS -M ").append("ict@nabi.res.in").append("\n");

            } else {
                input.append("#PBS -M ").append(emailid).append(",ict@nabi.res.in").append("\n");
            }
            System.out.println("The contents to be written to script are: " + input.toString());

        } catch (NullPointerException e) {

            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of createnewfile");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println(e);
            return "dataappendingexception";
        }
        try {

            File newFile = new File(job_fldr + "/" + scriptname + "_turbo_script.sh");
            if (newFile.exists()) {
                System.out.println("File alreday exists.");
                return "FileExists";
            } else {
                newFile.createNewFile();
                UserPrincipal owner;
                owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
                BufferedWriter output = new BufferedWriter(new FileWriter(newFile));
                output.write(input.toString());
                output.close();
                Files.setOwner(newFile.toPath(), owner);
                GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                Files.getFileAttributeView(newFile.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                newFile.setWritable(true);
                newFile.setReadable(true);
                newFile.setExecutable(true);
                check = executecmdstring(hostName, userName, password, "dos2unix " + job_fldr + "/" + scriptname + "_turbo_script.sh" + ";echo $?");
                if (check.equals("0")) {

                    System.out.println("\\n---------------------------------------------------\n\n");
                    System.out.println("New script creation and DOS 2 UNIX conversion is successful");
                    System.out.println("\\n---------------------------------------------------\n\n");
                    return "FileCreationConversionSuccess";
                } else {
                    System.out.println("\\n---------------------------------------------------\n\n");
                    System.out.println("File creation is successful but DOS 2 UNIX conversion is unsuccessful");
                    System.out.println("\\n---------------------------------------------------\n\n");
                    return "FileCreationSuccessConversionFail";
                }
            }

        } catch (Exception e) {
            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of create new file writing code");
            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);
            return "FileCreationException";
        }
    }

    
    public synchronized String[] submitjob(String hostName, String userName, String password, String scriptpath, String jobname, String wtime, String nnodes, String ppn, String oploc, String errorloc, String start, String end, String abort, String emailid, String scriptpath1, String randomscript) {

        String[] result;
        result = null;
        System.out.println("\n\nInside submit function, the value of start is" + start + " end is " + end + " abort is" + abort + "\n\n");
        StringBuilder input = new StringBuilder();
        input.append("qsub ");

        try {

            if (randomscript.equals("Yes")) {

                if (jobname.equals("")) {
                } else {

                    input.append("-N ").append(jobname).append(" ");
                }

                if (wtime.equals("")) {
                } else {
                    input.append("-l walltime=").append(wtime).append(" ");
                }

                if (nnodes.equals("") && ppn.equals("")) {
                } else {
                    input.append("-l nodes=").append(nnodes).append(":ppn=").append(ppn).append(" ");
                }

                if (oploc.equals("")) {
                } else {
                    input.append("-o ").append(oploc).append(" ");
                }
                if (errorloc.equals("")) {
                } else {
                    input.append("-e ").append(errorloc).append(" ");
                }
                if (start.equals("") && end.equals("") && abort.equals("")) {
                } else {
                    input.append("-m ").append(start).append(end).append(abort).append(" ");
                }
                if (emailid.equals("")) {
                    input.append("-M ").append("ict@nabi.res.in").append(" ");

                } else {
                    input.append("-M ").append(emailid).append(",ict@nabi.res.in").append(" ");
                }
                input.append(scriptpath);
            } else if (randomscript.equals("No")) {
                input.append(scriptpath1);
            }
            result = ref.executecmd(hostName, userName, password, input.toString() + "; echo $?");
            System.out.println("\n\nSubmit Command as formed in Submitjob method is:" + input.toString() + "\n\n");

        } catch (Exception e) {

            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("\\nInside Exception of Submit Job\n\n");
            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);

        }
        return result;
    }

    public synchronized String[] quicksubmit(String hostName, String userName, String password, String scriptpath) {

        String[] result;
        result = null;
        System.out.println("\n\nInside quicksubmit function,\n");
        StringBuilder input = new StringBuilder();
        input.append("qsub ");

        try {
            input.append(scriptpath);
            result = ref.executecmd(hostName, userName, password, input.toString() + "; echo $?");
            System.out.println("\n\nCommand as formed in QuickSubmit:" + input.toString() + "\n\n");
        } catch (Exception e) {

            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("\\nInside Exception of quickSubmit \n\n");
            System.out.println("\\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);

        }
        return result;

    }

    public synchronized String[] displayFile(String hostName, String userName, String password, String filePath) {
        String filename = ref.executecmdstring(hostName, userName, password, "file " + filePath + " | awk -F: '{print $1}'");
        String filename1 = ref.executecmdstring(hostName, userName, password, "file " + filePath + " | awk -F: '{print $1}'");

        if ((filename.equals(filePath) || filename1.equals(filePath))) {
            return ref.executecmd(hostName, userName, password, "cat " + filePath);

        } else {
            return null;
        }
    }

    public synchronized boolean editFile(String hostName, String userName, String password, String script, String filepath, String homedir) {

        int j = 0;
        List<Integer> extrctd_trgt_path = new ArrayList<Integer>();
        char sep = '/';

        try {

            String check = null;
            File f = new File(filepath);
            String fname = f.getName();
            System.out.println("\n\nEdited file is " + script + "\n\n");
            System.out.println("\nFile to be modified is :" + fname + "\n" + "Script:" + script.trim() + "\n" + "File Path of file to be changed is:" + filepath + "\n");
            for (int i = 0; i <= filepath.length(); i++) {

                if (filepath.charAt(i) == sep) {
                    extrctd_trgt_path.add(i);
                    j++;
                } else {
                }
                if (j == 3) {

                    break;
                }

            }
            String extrctd_trgt_path_string = filepath.substring(extrctd_trgt_path.get(0), extrctd_trgt_path.get(2));
            File newFile = new File(filepath);
            File dfltfile = new File(homedir);
            File trgetfile = new File(extrctd_trgt_path_string.toString());
            UserPrincipal trget_owner;
            trget_owner = Files.getOwner(trgetfile.toPath(), LinkOption.NOFOLLOW_LINKS);
            UserPrincipal owner;
            owner = Files.getOwner(dfltfile.toPath(), LinkOption.NOFOLLOW_LINKS);
            if (trget_owner.equals(owner)) {


                if (fname.equals(".bashrc")) {
                    check = ref.executecmdstring(hostName, userName, password, "echo \"" + script + "\" > " + filepath + " ; source ~/.bashrc ; echo $?");
                } else {

                    BufferedWriter writer = new BufferedWriter(new FileWriter(filepath));
                    writer.write(script);
                    writer.close();
                    Files.setOwner(newFile.toPath(), owner);
                    GroupPrincipal group = Files.readAttributes(dfltfile.toPath(), PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS).group();
                    Files.getFileAttributeView(newFile.toPath(), PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS).setGroup(group);
                    newFile.setWritable(true);
                    newFile.setReadable(true);
                    newFile.setExecutable(true);
                    check = ref.executecmdstring(hostName, userName, password, "dos2unix " + filepath + "; echo $?");

                }

                if (check.equals("0")) {
                    System.out.println("after dos2unix");
                    System.out.println("DOS2UNIX conversion in edit file function is successful");
                    return true;
                } else {
                    return false;
                }
            } else {

                System.out.println("###########################################");
                System.out.println("Permission Conflicts between target location and dflt location in editfile code hence returning false;");
                System.out.println("###########################################");
                return false;

            }

        } catch (Exception e1) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of editfile method");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            return false;

        }

    }

    public synchronized boolean makeblastdata(String hostName, String userName, String password, String mpipath, BigDecimal np, String mpiblastpath, String blastbinary, String blastdb, String inputfilepath, String evalue, String outfmt, String numdesc, String numalgn, String outputfilepath, String scriptname, String scriptloc) {

        StringBuilder input = new StringBuilder();
        input.append("\n");
        input.append("date" + "\n\n");
        try {

            if (mpipath.equals("")) {
            } else {

                input.append(mpipath).append(" " + "-np" + " ").append(np).append(" ");
            }

            if (mpiblastpath.equals("")) {
            } else {

                input.append(mpiblastpath).append(" ");
            }

            if (blastbinary.equals("")) {
            } else {

                input.append("-p" + " ").append(blastbinary).append(" ");
            }
            if (blastdb.equals("")) {
            } else {

                input.append("-d" + " ").append(blastdb).append(" ");

            }

            if (inputfilepath.equals("")) {
                System.out.println("No input file path");
                return false;
            } else {
                input.append("-i" + " ").append(inputfilepath).append(" ");

            }

            if (evalue.equals("")) {
                input.append("-e" + " " + "10").append(" ");

            } else {

                input.append("-e" + " ").append(evalue).append(" ");
            }

            if (outfmt.equals("")) {
                input.append("-m" + " " + "9").append(" ");

            } else {

                input.append("-m" + " ").append(outfmt).append(" ");
            }

            if (numdesc.equals("")) {
                input.append("-v" + " " + "1").append(" ");

            } else {

                input.append("-v" + " ").append(numdesc).append(" ");
            }

            if (numalgn.equals("")) {
                input.append("-b" + " " + "1").append(" ");

            } else {

                input.append("-b" + " ").append(numalgn).append(" ");
            }

            if (outputfilepath.equals("")) {
                System.out.println("No output file path");
                return false;
            } else {

                input.append("-o" + " ").append(outputfilepath).append("_\\$PBS_JOBID.txt" + " ");

            }
            input.append("\n\n");
            input.append("date" + "\n\n");
        } catch (NullPointerException e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception block of makeblastdb");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println(e);
        }
        System.out.println("\n\nThis is new blast specific data" + input.toString() + "\n\n");
        System.out.println("This is the exact path to the script: " + scriptloc + scriptname);
        try {

            File file = new File(scriptloc + scriptname);
            if (file.exists()) {

                String append_cmd = "echo \"" + input.toString() + "\" >> " + scriptloc + scriptname + " && dos2unix " + scriptloc + scriptname + "; echo $?";
                String stat = ref.executecmdstring(hostName, userName, password, append_cmd);

                if (stat.equals("0")) {

                    System.out.println("\n\nSuccessfully apended mpiblast data at the end of script in mpiblast method\n\n");
                    input = null;
                    return true;

                } else {
                    System.out.println("\n\nApending of mpiblast data at the end of script is unsuccessful\n\n");
                    input = null;
                    return false;

                }
            } else {

                System.out.println("\n\nFile has been deleted intermittently, hence rolling back the changes\n\n");
                input = null;
                return false;

            }

        } catch (Exception e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of file writing code of makeblastdb");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);
            return false;
        }


    }
    
    public synchronized boolean maketurboblastdata(String hostName, String userName, String password, String mpipath, BigDecimal np, String mpiblastpath, String blastbinary, String blastdb, String inputfilepath, String evalue, String outfmt, String numdesc, String numalgn, String outputfilepath, String scriptloc) {
        
        File file = new File(scriptloc);
        StringBuilder input = new StringBuilder();
        input.append("\n");
        input.append("date" + "\n");
        try {

            if (mpipath.equals("")) {
            } else {

                input.append(mpipath).append(" " + "-np" + " ").append(np).append(" ");
            }

            if (mpiblastpath.equals("")) {
            } else {

                input.append(mpiblastpath).append(" ");
            }

            if (blastbinary.equals("")) {
            } else {

                input.append("-p" + " ").append(blastbinary).append(" ");
            }
            if (blastdb.equals("")) {
            } else {

                input.append("-d" + " ").append(blastdb).append(" ");

            }

            if (inputfilepath.equals("")) {
                System.out.println("No input file path");
                return false;
            } else {
                input.append("-i" + " ").append(inputfilepath).append(" ");

            }

            if (evalue.equals("")) {
                input.append("-e" + " " + "10").append(" ");

            } else {

                input.append("-e" + " ").append(evalue).append(" ");
            }

            if (outfmt.equals("")) {
                input.append("-m" + " " + "9").append(" ");

            } else {

                input.append("-m" + " ").append(outfmt).append(" ");
            }

            if (numdesc.equals("")) {
                input.append("-v" + " " + "1").append(" ");

            } else {

                input.append("-v" + " ").append(numdesc).append(" ");
            }

            if (numalgn.equals("")) {
                input.append("-b" + " " + "1").append(" ");

            } else {

                input.append("-b" + " ").append(numalgn).append(" ");
            }

            if (outputfilepath.equals("")) {
                System.out.println("No output file path");
                return false;
            } else {

                input.append("-o" + " ").append(outputfilepath).append("_$PBS_JOBID.txt" + " ");

            }
            input.append("\n");
            input.append("date" + "\n");
        } catch (NullPointerException e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println("Inside exception block of maketurboblastdb");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            System.out.println(e);
        }
        System.out.println("\n\nThis is the new blast data" + input.toString() + "\n");
        System.out.println("Exact path to the script from maketurboblastdata function : " + scriptloc);
        try {
            
            if (file.exists()) {
                String append_cmd = "echo \"" + input.toString() + "\" >> " + scriptloc + " && dos2unix " + scriptloc + "; echo $?";
                String stat = ref.executecmdstring(hostName, userName, password, append_cmd);

                if (stat.equals("0")) {

                    System.out.println("\n\nSuccessfully apended mpiblast data at the end of script in mpiblast method\n\n");
                    input = null;
                    return true;

                } else {
                    System.out.println("\n\nApending of mpiblast data at the end of script is unsuccessful\n\n");
                    input = null;
                    return false;

                }
            } else {

                System.out.println("\n\nFile has been deleted intermittently, hence rolling back the changes\n\n");
                input = null;
                return false;

            }

        } catch (Exception e) {
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            System.out.println("Inside exception of file writing code of maketurboblastdb");
            System.out.println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
            e.printStackTrace(System.err);
            return false;
        }


    }


    public String executecommand(String hostname, String username, String password, String cmd) {


        StringBuilder sb = null;
        ArrayList command_output = new ArrayList<String>();
        int i = 0;

        try {
            conn = new Connection(hostname);
            conn.connect();
            conn.authenticateWithPassword(username, password);
            sess = conn.openSession();
            sess.execCommand(cmd);
            InputStream stdout = new StreamGobbler(sess.getStdout());
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                command_output.add(line);

                System.out.println(command_output.get(i));
                i++;

            }

            i = command_output.size();
            i--;
            System.out.println(command_output.get(i));

            if (command_output.get(i).equals("0")) {

                System.out.println("Command successfull in executecommand() function of SSH.java");
                br.close();
                stdout.close();
                sess.close();
                conn.close();
                return "true";

            } else {
                System.out.println("Command failure in executecommand() function of SSH.java");
                br.close();
                stdout.close();
                sess.close();
                conn.close();
                return "false";
            }

        } catch (Exception e) {
            System.out.println("Inside exception of executcommand");
            System.out.println(e.toString());
            return "false";
        }
    }

    public String getHostname(String hostName, String userName, String password) {
        return ref.executecmdstring(hostName, userName, password, "echo $HOSTNAME");
    }
}
