/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * This file is second part of create script action file in job script package. It perfroms the 
 * job of entering mpiblast specific data into created script. 
 * 
 */
package mpiBlast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import logic_core.Core_func;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author parichit
 */
public class mpiblastAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String BLAST_ENTRY_SUCCESS = "success";
    private static final String BLAST_ENTRY_FAIL = "fail";
    private static final String UPLD_FAIL = "upldfail";
    private static final String UPLD_SUCCESS = "upldsuccess";

    @Override
    public synchronized ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        mpiblastFormBean editedform = (mpiblastFormBean) form;
        FormFile localfilename = editedform.getUploadFile();
        String inputpath = editedform.getInputfilepath();
        String outputpath = editedform.getOutputfilepath();
        String blastbinary = editedform.getBlastbinary();
        String blastdb = editedform.getBlastdb();
        String evalue = editedform.getEvalue();
        String outfmt = editedform.getOutfmt();
        String numdesc = editedform.getNumdesc();
        String numalgn = editedform.getNumalgn();
        String button = editedform.getButton();
        String mpipath = "empty";
        String mpiblastpath = "empty";
        BigDecimal op1;
        BigDecimal op2;
        BigDecimal np = null;
        String scriptname = null;
        String scriptloc = null;
        String ppn = null;
        String nnodes = null;
        Core_func refssh = Core_func.getInstance();
        String username = session.getAttribute("username").toString();
        String hostname = session.getAttribute("hostip").toString();
        String password = session.getAttribute("password").toString();
        String homedir = session.getAttribute("homedir").toString();
        String job_name = session.getAttribute("jobname").toString();
        String strLine = "";
        String[] result;
        String jobid = null;
        String jobfldr = homedir + "WIMPIHome/" + job_name + "/";

        if (button.equals("SAVE & SUBMIT")) {

            try {

                System.out.println("-------------------------------");
                System.out.println("location of upload folder on server is : " + jobfldr);
                System.out.println("-------------------------------");
                String filename = localfilename.getFileName();
                System.out.println("-------------------------------");
                System.out.println("Just before calling upload logic_core.java from mpblast action");
                System.out.println("-------------------------------");

                if (filename.contains(" ")) {
                    filename = filename.replace(" ", "_");

                }
                if (refssh.fileUpload(hostname, username, password, filename, jobfldr, localfilename, homedir, "appliupld")) {
                    //session.setAttribute("action_stat", "yesupld");
                    session.setAttribute("ippath", homedir + "WIMPIHome/" + job_name + "/" + localfilename);
                    //return mapping.findForward(UPLD_SUCCESS);

                } else {
                    session.setAttribute("action_stat", "noupld");
                    session.removeAttribute("ip_path");
                    return mapping.findForward(UPLD_FAIL);

                }

            } catch (Exception e) {
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                System.out.println("Inside Exception block of mpiblast upload file");
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                session.setAttribute("action_stat", "excepupld");
                session.removeAttribute("ip_path");
                return mapping.findForward(UPLD_FAIL);
            }

            try {

                File conf_file = new File("/etc/.WImpiBLAST.conf");
                if (conf_file.getAbsoluteFile().exists()) {
                    FileInputStream finputstream = new FileInputStream("/etc/.WImpiBLAST.conf");
                    BufferedReader br = new BufferedReader(new InputStreamReader(finputstream));
                    System.out.println("mpirun path before calculation is : " + mpipath);
                    System.out.println("mpiblast path before calculation is : " + mpiblastpath);
                    if (mpiblastpath.equals("empty") || mpipath.equals("empty")) {
                        while ((strLine = br.readLine()) != null) {

                            if (strLine.startsWith("mpirun_path")) {
                                int len = strLine.length();
                                mpipath = strLine.substring(13, len - 1).toString().trim();

                                if (mpipath.equals("") || mpipath.isEmpty()) {
                                    session.setAttribute("action_stat", "param_fail");
                                    session.removeAttribute("scriptname");
                                    session.removeAttribute("nnodes");
                                    session.removeAttribute("ppn");
                                    session.removeAttribute("jobname");
                                    scriptname = null;
                                    session.setAttribute("action_stat", "param_fail");
                                    return mapping.findForward(BLAST_ENTRY_FAIL);
                                }

                            } else if (strLine.startsWith("mpiblast_path")) {

                                int len = strLine.length();
                                mpiblastpath = strLine.substring(15, len - 1).toString().trim();

                                if (mpiblastpath.equals("") || mpiblastpath.isEmpty()) {
                                    session.setAttribute("action_stat", "param_fail");
                                    session.removeAttribute("scriptname");
                                    session.removeAttribute("nnodes");
                                    session.removeAttribute("ppn");
                                    session.removeAttribute("jobname");
                                    scriptname = null;
                                    session.setAttribute("action_stat", "param_fail");
                                    return mapping.findForward(BLAST_ENTRY_FAIL);
                                }

                            }

                        }

                        System.out.println("------------------------------------------------------------------\n");
                        System.out.println("mpirun path after calculation is : " + mpipath);
                        System.out.println("------------------------------------------------------------------\n");
                        System.out.println("mpiblast path after calculation is : " + mpiblastpath);
                        System.out.println("------------------------------------------------------------------\n");

                    }

                } else {
                    System.out.println("------------------------------------------------------------------\n");
                    System.out.println("WImpiBLAST conf file undetected in mpiblast action class file , so returning false");
                    System.out.println("------------------------------------------------------------------\n");
                    session.setAttribute("action_stat", "param_fail");
                    return mapping.findForward(BLAST_ENTRY_FAIL);

                }

                //scriptloc = session.getAttribute("scriptloc").toString();
                scriptname = session.getAttribute("scriptname").toString();
                nnodes = session.getAttribute("nnodes").toString();
                ppn = session.getAttribute("ppn").toString();
                System.out.println("From create script scriptname: , nonodes, ppn" + scriptname + " " + nnodes + " " + ppn);
                op1 = new BigDecimal(nnodes);
                op2 = new BigDecimal(ppn);
                np = op1.multiply(op2);
                //System.out.println(np);


                if (refssh.makeblastdata(hostname, username, password, mpipath, np, mpiblastpath, blastbinary, blastdb, inputpath, evalue, outfmt, numalgn, numdesc, outputpath, scriptname, jobfldr)) {
                    System.out.println("------------------------------------------------------------------\n");
                    System.out.println("Appending of mpiblast specific data to script is successfule\n");
                    System.out.println("------------------------------------------------------------------\n");
                    session.removeAttribute("nnodes");
                    session.removeAttribute("ppn");
                    result = refssh.quicksubmit(hostname, username, password, jobfldr + scriptname);
                    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                    //System.out.println("the result length after executing submitjob function is " + result.length + " " + result[0] + " " + result[1]);
                    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                    if (result.length != 2) {

                        System.out.println("------------------------------------------");
                        System.out.println("Job Submission is unsucessful");
                        System.out.println("------------------------------------------");
                        session.setAttribute("action_stat", "jobfail");
                        return mapping.findForward(BLAST_ENTRY_FAIL);
                    } else {
                        if (result.length == 2) {

                            if (result[1].equals("0")) {
                                jobid = result[0];
                                System.out.println("----------------------------------------------------");
                                System.out.println("The Job submission successful with jobid!!!" + jobid);
                                System.out.println("----------------------------------------------------");
                                session.setAttribute("action_stat", "jobsuccess");
                                //session.removeAttribute("jobname");
                                session.removeAttribute("scriptname");
                                return mapping.findForward(BLAST_ENTRY_SUCCESS);

                            } else {
                                System.out.println("-------------------------------------------");
                                System.out.println("The Job submission unsuccessful !!!");
                                System.out.println("-------------------------------------------");
                                session.setAttribute("action_stat", "jobfail");
                                return mapping.findForward(BLAST_ENTRY_FAIL);
                            }

                        }
                    }


                } else {
                    System.out.println("------------------------------------------------------------------\n");
                    System.out.println("Appending of mpiblast data at the end of file is unsuccessful\n");
                    System.out.println("------------------------------------------------------------------\n");
                    session.removeAttribute("scriptname");
                    session.removeAttribute("nnodes");
                    session.removeAttribute("ppn");
                    session.removeAttribute("jobname");
                    session.setAttribute("action_stat", "no");
                    return mapping.findForward(BLAST_ENTRY_FAIL);

                }

            } catch (Exception e) {

                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                System.out.println("Inside exceptionn code of Mpiblast--Action--Class");
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                System.out.println(e);
                session.setAttribute("action_stat", "ex");
                session.removeAttribute("scriptname");
                session.removeAttribute("nnodes");
                session.removeAttribute("ppn");
                session.removeAttribute("jobname");
                return mapping.findForward(BLAST_ENTRY_FAIL);
            }

        }

        return mapping.findForward(BLAST_ENTRY_SUCCESS);

    }
}
