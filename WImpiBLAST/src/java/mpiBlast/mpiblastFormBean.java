package mpiBlast;

import org.apache.struts.upload.FormFile;

public class mpiblastFormBean extends org.apache.struts.action.ActionForm {

    private String filename = "";
    private String inputfilepath = "";
    private String outputfilepath = "";
    private String button = "";
    private String blastbinary = "";
    private String blastdb = "";
    private FormFile uploadFile = null;
    private String evalue = "";
    private String outfmt = "";
    private String numdesc = "";
    private String numalgn = "";

    public String getEvalue() {
        return evalue;
    }

    public void setEvalue(String evalue) {
        this.evalue = evalue;
    }

    public String getOutfmt() {
        return outfmt;
    }

    public void setOutfmt(String outfmt) {
        this.outfmt = outfmt;
    }

    public String getNumdesc() {
        return numdesc;
    }

    public void setNumdesc(String numdesc) {
        this.numdesc = numdesc;
    }

    public String getNumalgn() {
        return numalgn;
    }

    public void setNumalgn(String numalgn) {
        this.numalgn = numalgn;
    }

    public FormFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(FormFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getBlastdb() {
        return blastdb;
    }

    public void setBlastdb(String blastdb) {
        this.blastdb = blastdb;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getInputfilepath() {
        return inputfilepath;
    }

    public void setInputfilepath(String inputfilepath) {
        this.inputfilepath = inputfilepath;
    }

    public String getOutputfilepath() {
        return outputfilepath;
    }

    public void setOutputfilepath(String outputfilepath) {
        this.outputfilepath = outputfilepath;
    }

    

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getBlastbinary() {
        return blastbinary;
    }

    public void setBlastbinary(String blastbinary) {
        this.blastbinary = blastbinary;
    }
}
