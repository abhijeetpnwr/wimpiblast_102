/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * 
 * This java file contains the list of commands used in generating job list and related parameters
 * by executing these commands on system. These commands are called by appropriate jsp files
 * such as joblist.jsp and jobmanage.jsp when user open these files in browser, this file is used
 * to get associated commands.
 * 
 */

package GlobalParam;

public interface GlobalParam {
 
    /**** JOB Relevant Description ****/
    
    static final String JOBID1 = "qstat -u ";
    static final String JOBID2 = " |awk '{print$1}'| sed -n '6,$p'";
    
    static final String JOB_UNAME1 = "qstat -u ";
    static final String JOB_UNAME2 = " |awk '{print$2}'| sed -n '6,$p'";
    
    static final String JOBQUEUE1 = "qstat -u ";
    static final String JOBQUEUE2 = " | awk '{print$3}'| sed -n '6,$p'";
    
    static final String JOBNAME1 = "qstat -u ";
    static final String JOBNAME2 = " | awk '{print$4}'| sed -n '6,$p'";
    
    static final String JOB_NODES1 = "qstat -u ";
    static final String JOB_NODES2 = " | awk '{print$6}'| sed -n '6,$p'";
    
    static final String JOB_PPN1 = "qstat -u ";
    static final String JOB_PPN2 = " | awk '{print$7}'| sed -n '6,$p'";
    
    static final String JOB_STATE1 = "qstat -u ";
    static final String JOB_STATE2 = " | awk '{print$10}'| sed -n '6,$p'";
    
    static final String JOB_ELAP_TIME1 = "qstat -u ";
    static final String JOB_ELAP_TIME2 = " | awk '{print$11}'| sed -n '6,$p'";
    
    static final String JOB_NODE_NAME1="qstat -u "; 
    static final String JOB_NODE_NAME2=" | awk '{print$12}'| sed -n '6,$p'"; 
    
    static final String JOB_TIMEREQD1 = "qstat -u ";
    static final String JOB_TIMEREQD2 = " -a | awk '{print$9}'| sed -n '6,$p'";
    
    static final String JOBID_short1 = "qstat -u ";
    static final String JOBID_short2 = " | awk '{print$1}'| sed '1,5d'";
    
    static final String JOBNAME_short1 = "qstat -u ";
    static final String JOBNAME_short2 = " | awk '{print$4}'| sed '1,5d'";
    
    static final String JOB_UNAME_short1 = "qstat -u ";
    static final String JOB_UNAME_short2 = "|awk '{print$2}'| sed '1,5d' ";
    
    static final String JOB_STATE_short1 = "qstat -u ";
    static final String JOB_STATE_short2 = " | awk '{print$10}'| sed '1,5d'";
    
    static final String JOBQUEUE_short1 = "qstat -u ";
    static final String JOBQUEUE_short2 = " | awk '{print$3}'| sed '1,5d'";
    
    static final String JOB_MEMREQD1 = "qstat -u ";
    static final String JOB_MEMREQD2 = " -n1 | awk '{print$8}'| sed -n '6,$p'";
    
    
    static final String numqueues = "qstat -Q | awk '{print $1}' | sed -n '1,2!p'";
    static final String queueName = "qstat -Q | awk '{print $1}' | sed -n '1,2!p'";
    static final String totRunJob = "qstat -Q | awk '{print $3}' | sed -n '1,2!p'";
    static final String queStrted = "qstat -Q | awk '{print $5}' | sed -n '1,2!p'";
    static final String queEnbld = "qstat -Q | awk '{print $4}' | sed -n '1,2!p'";
    static final String queType = "qstat -Q | awk '{print $12}' | sed -n '1,2!p'";
    static final String jobHld = "qstat -Q | awk '{print $8}' | sed -n '1,2!p'";
    static final String jobRun = "qstat -Q | awk '{print $7}' | sed -n '1,2!p'";
    static final String jobQued = "qstat -Q | awk '{print $6}' | sed -n '1,2!p'";
    
    static final String adm_JOBID = "qstat -n1 |awk '{print$1}'| sed '1,5d'";
    static final String adm_UNAME = "qstat -n1 |awk '{print$2}'| sed '1,5d'";
    static final String adm_JOBQUEUE = "qstat -n1 |awk '{print$3}'| sed '1,5d'";
    static final String adm_JOBNAME = "qstat -n1 |awk '{print$4}'| sed '1,5d'";
    static final String adm_PPN = "qstat -n1 |awk '{print$7}'| sed '1,5d'";
    static final String adm_MEM = "qstat -n1 |awk '{print$8}'| sed '1,5d'";
    static final String adm_TIME = "qstat -n1 |awk '{print$9}'| sed '1,5d'";
    static final String adm_STATE = "qstat -n1 |awk '{print$10}'| sed '1,5d'";
    static final String adm_ELAPTIME = "qstat -n1 |awk '{print$11}'| sed '1,5d'";
    
}
